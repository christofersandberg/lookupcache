The objective of this assignment is to design and implement a component which encapsulates a data structure which will used to lookup reference data in a latency-sensitive data processing pipeline.

The data which needs to be ingested is included in the attached file: data.json.  It is organized as follows:

Outer Array of Organization Objects
Organization Objects contain an array of parameter objects, keyed by orgKey
ParameterObjects contain an array of 1...n segment objects, keyed by paramKey
SegmentConfig objects are keyed by the parameter values which apply to that segment.  These can be an empty string, a single value, or a newline-delimited
string of values. Internally, Segment Objects have 1 field: id

Functionally, the following interface must be supported:

public interface DataCache{
	public SegmentConfig[] getSegmentFor(final String orgKey, final String paramKey)
	public SegmentConfig[] getSegmentFor(final String orgKey, final String paramKey, final String paramValKey)
}

The first function will return an array of SegmentConfig objects given an orgKey and a parameterKey.  This call should return all SegmentConfigs associated
with that org and parameter, and with an empty parameterValue

The second function will return an array of SegmentConfig objects given an orgKey, a parameterKey, and a parameterVal key.  This call should return all SegmentConfigs associated
with that org, parameter, and parameter value where a parameter value is any single value in a list of 1 or more values.

Example:

Given the following JSON Structure:

[
{
    "org1": [
      {
        "paramName1": [
          {
            "paramVal1": {
              "segmentId": "seg_1234",
              "segmentTTL": 720,
              "ignored": false,
              "name": "Interest - Education Seekers - School"
            }
          },
          {
            "paramVal2\nparamVal3\nparamVal4\nsparamVal5": {
              "segmentId": "seg_abcd",
              "segmentTTL": 720,
              "ignored": false,
              "name": "Interest - Education Seekers"
            }
          }
        ]
      },
      {
        "paramName2": [
          {
            "": {
              "segmentId": "seg_xyz",
              "segmentTTL": 720,
              "ignored": false,
              "name": "Interest - Education Seekers - School"
            }
          }
        ]
      }
    ]
}
]

The query getSegmentFor("org1", "paramName1")  will return an empty SegmentConfig array.
The query getSegmentFor("org1", "paramName1", paramVal1")  will return a 1-element SegmentConfigArray containing a SegmentConfig object for seg_1234
The query getSegmentFor("org1", "paramName1", <<"paramVal2" |  "paramVal3" |  "paramVal4" |  "paramVal5">> )  will return a 1-element SegmentConfigArray containing a SegmentConfig object for seg_abcd
The query getSegmentFor("org1", "paramName2")  will return a 1-element SegmentConfigArray containing a SegmentConfig object for seg_xyz
The query getSegmentFor("org1", "paramName2", "")  will return a 1-element SegmentConfigArray containing a SegmentConfig object for seg_xyz
The query getSegmentFor("org1", "paramName2", <<any value other than an empty string>>) will return an empty SegmentConfig array.


Other requirements:

-The data structure should be as small as possible in memory
-The design should be optimized for speed of lookup (i.e., the methods defined in the interface).
-The design should be optimized to be as memory stable as possible for lookups; meaning high rates of lookup calls should not
 generate memory churn and significant gc activity.
-All solutions should only use the Java 7 JDK and the Jettison library for JSON parsing (if you choose).  You may use
-Please include instructions for how to build and run your solution.
-Solutions should be submitted to code@eyeota.com within 5 calendar days of receiving the problem.
-If you have any questions, please submit them to technology@eyeota.com


